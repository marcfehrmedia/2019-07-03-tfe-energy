import React from 'react';
import { Link } from 'gatsby';
import Img from 'gatsby-image';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
	card: {
		maxWidth: '100%',
	},
	media: {
		height: 280,
		maxHeight: 'calc(100vw/16*9)'
	},
	noLink: {
		textDecoration: 'none !important',
		fontSize: '1.2rem'
	}
});
const linkStyle = {
	color: 'inherit',
	textDecoration: 'none',
}

export default function CardComponent({data, linkTo}) {
	const projectData = data.node;
	const classes = useStyles();
	
	return (
		<Card className={classes.card}>
			<Link to={`${linkTo}`}
						style={linkStyle}
			>
				<CardActionArea>
					<Img
						className={classes.media}
						fluid={projectData.image.fluid}
						// title={projectData.image.file.name}
					/>
					<CardContent>
						<Typography gutterBottom
						            variant="h5"
						            component="h2"
						            className={classes.noLink}
						>
							{projectData.title}
						</Typography>
						<Typography variant="body2" color="textSecondary" component="p">
							{projectData.description}
						</Typography>
					</CardContent>
				</CardActionArea>
			</Link>
			<CardActions>
				<Link to={`${linkTo}`}
				      style={linkStyle}
				>
					<Button size="small" color="secondary" style={{fontWeight: 800}}>
						{projectData.type ? (
							<span>Go to {projectData.type}</span>
							) : (
							<span>Read more</span>
							)
						}

					</Button>
				</Link>
				<a href={`https://twitter.com/intent/tweet?text=https://tfe-energy.netlify.com${linkTo}`}
			     style={linkStyle}
				   target={'_blank'}
				   rel="noopener noreferrer"
				>
					<Button size="small" color="primary">
						<span><i className='ui icon twitter' />Share</span>
					</Button>
				</a>
				<a href={`https://www.facebook.com/sharer/sharer.php?u=https://tfe-energy.netlify.com${linkTo}`}
				   style={linkStyle}
				   target={'_blank'}
				   rel="noopener noreferrer"
				>
					<Button size="small" color="primary">
						<span><i className='ui icon facebook' />Share</span>
					</Button>
				</a>
				<a href={`https://www.linkedin.com/shareArticle?mini=true&url=https://www.tfe.energy${linkTo}&image=${projectData.image.file.url}&title=${projectData.title}`}
				   style={linkStyle}
				   target={'_blank'}
				   rel="noopener noreferrer"
				>
					<Button size="small" color="primary">
						<span><i className='ui icon linkedin' />Share</span>
					</Button>
				</a>
			</CardActions>
		</Card>
	);
}
