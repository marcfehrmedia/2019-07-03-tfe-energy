import React from 'react';
import { Grid } from 'semantic-ui-react';
import Img from 'gatsby-image';
import styled from 'styled-components';

export default function GridComponent(props) {
	const StyledImage = styled.div`
		img:hover {
			-webkit-animation: heartbeat 1.5s ease-in-out infinite both;
      animation: heartbeat 1.5s ease-in-out infinite both;
		}
	`;

	const gridElements = props.data.map((el,i) => (
		<Grid.Column style={{textAlign: 'center'}} white key={`gridelement-${i}`}>
			<a href={el.node.url}
	  		 target={'_blank'}
			   rel="noopener noreferrer"
			>
				<StyledImage>
					<Img
						fluid={el.node.image.fluid}
						style={{overflow: 'visible', margin: '1rem'}}
						imgStyle={{
							width: '100%',
							objectFit: 'contain'
						}}
					/>
				</StyledImage>
			</a>
		</Grid.Column>
	))

	return (
		<Grid centered
					stackable={props.stackable}
					doubling={props.doubling}
		      relaxed={props.relaxed}
		      style={props.styles}
		      className={`${props.gradient ? 'gradient-animation': ''} ${props.columnBackground ? props.columnBackground : ''}`}
		>
			<Grid.Row centered columns={3}>
				{ gridElements }
			</Grid.Row>
		</Grid>
	);
}
